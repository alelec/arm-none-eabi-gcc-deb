# arm-none-eabi-gcc-deb
Automated x86_64 deb package builder for arm-none-eabi-gcc.

Based directly on the script from https://askubuntu.com/a/1371525/1379646

Creates deb to install / uninstall gcc from https://developer.arm.com/tools-and-software/open-source-software/developer-tools/gnu-toolchain/downloads

